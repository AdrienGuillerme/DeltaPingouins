﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// just a class to store the next level to be loaded by the loading scene

public class LoadManager : MonoBehaviour {
    public static string LevelToLoad = "Intro";
    public static string previousLevel = "menu";

    // used when levelToLoad = mission
    public static string mapName = "";
    public static List<Penguin> penguins = new List<Penguin>();
    public static int lifeLevel = 0;
    public static int mvmtLevel = 0;
    public static int dmgLevel = 0;

    // used when levelToLoad = Intro
    public static string playerName = "player";
}
