﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeListBehaviour : MonoBehaviour {

    public QGManager manager;
    public GameObject upgradeDetailPrefab;
    public GameObject parent;
    private GameObject[] elements;

    public void Init()
    {
        List<Upgrade> upgrades = manager.GetUpgrades();
        int count = 0;
        float x = -455;
        foreach (Upgrade m in upgrades)
        {
            GameObject r = Instantiate(upgradeDetailPrefab, parent.transform);
            r.transform.localScale = new Vector3(1f, 1f, 1f);
            r.transform.localPosition = new Vector3(x, 0, 0);
            UpgradeButton script = r.GetComponent<UpgradeButton>();
            script.manager = manager;
            script.upgrade = m;
            script.parent = r;
            script.b = r.GetComponent<Button>();

            //set attributes
            Transform child = r.transform.Find("Name");
            Text name = child.GetComponent<Text>();
            child = r.transform.Find("Level");
            Text level = child.GetComponent<Text>();
            child = r.transform.Find("Description");
            Text type = child.GetComponent<Text>();
            child = r.transform.Find("Price");
            Text price = child.GetComponent<Text>();
            name.text = UpgradeTypeToString(m.GetUpgradeType());
            level.text = "niveau " + m.GetLevel().ToString() + "/5";
            type.text = m.GetDescription();
            price.text = "Coût :\n" + m.GetPrice() + " sorbets";

            Button b = r.GetComponent<Button>();
            if (m.GetPrice() <= manager.GetSorbets())
            {
                b.interactable = true;
            } else
            {
                b.interactable = false;
            }

            // increment position and change parent size when needed
            x += 220;
            count += 220;
            if (count > 1150)
            {
                RectTransform rt = parent.GetComponent<RectTransform>();
                rt.sizeDelta += new Vector2(220f, 0);
                x -= 110;
            }
        }
        parent.transform.localPosition += new Vector3(count / 2, 0, 0);
    }

    public void Cancel()
    {
        RectTransform rt = parent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1150f, 400f);
    }

    public string UpgradeTypeToString(UpgradeType r)
    {
        switch (r)
        {
            case UpgradeType.life:
                return "Vie";
            case UpgradeType.dmg:
                return "Dégats";
            case UpgradeType.speed:
                return "Vitesse";
            default:
                return "";
        }
    }
}
