﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FallenListBehaviour : MonoBehaviour {

    public QGManager manager;
    public GameObject penguinNamePrefab;
    public GameObject parent;
    private GameObject[] elements;

    public void Init()
    {
        List<Penguin> deads = manager.GetDeadPenguins();
        int count = 0;
        float y = 140;
        foreach (Penguin p in deads)
        {
            GameObject r = Instantiate(penguinNamePrefab, parent.transform);
            r.transform.localScale = new Vector3(1f, 1f, 1f);
            r.transform.localPosition = new Vector3(0, y, 0);

            //set attributes
            Transform child = r.transform.Find("Name");
            Text name = child.GetComponent<Text>();
            name.text = p.GetName();

            // increment position and change parent size when needed
            y -= 60;
            count += 60;
            if (count > 350)
            {
                RectTransform rt = parent.GetComponent<RectTransform>();
                rt.sizeDelta += new Vector2(0, 60f);
                y += 30;
            }
        }
        parent.transform.localPosition += new Vector3(0, count / 2, 0);
    }

    public void Cancel()
    {
        RectTransform rt = parent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(400f, 350f);
    }
}
