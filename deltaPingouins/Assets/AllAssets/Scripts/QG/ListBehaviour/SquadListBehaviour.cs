﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SquadListBehaviour : MonoBehaviour {

    public QGManager manager;
    public GameObject penguinDetailPrefab;
    public GameObject parent;
    private GameObject[] elements;
    private List<Button> buttons = new List<Button>();

    public void Init()
    {
        List<Penguin> squad = manager.GetSquad();
        int count = 0;
        float x = -455;
        foreach (Penguin p in squad)
        {
            GameObject r = Instantiate(penguinDetailPrefab, parent.transform);
            r.transform.localScale = new Vector3(1f, 1f, 1f);
            r.transform.localPosition = new Vector3(x, 0, 0);
            PenguinSelectButton script = r.GetComponent<PenguinSelectButton>();
            script.manager = manager;
            script.penguin = p;
            Button button = r.GetComponent<Button>();
            script.button = button;
            buttons.Add(button);

            //set attributes
            Transform child = r.transform.Find("Name");
            Text name = child.GetComponent<Text>();
            child = r.transform.Find("Spec");
            Text spec = child.GetComponent<Text>();
            child = r.transform.Find("Life");
            Text life = child.GetComponent<Text>();
            child = r.transform.Find("Mvmt");
            Text mvmt = child.GetComponent<Text>();
            child = r.transform.Find("Ability");
            Text ability = child.GetComponent<Text>();
            name.text = p.GetName();
            spec.text = p.GetSpec().ToString();
            life.text = p.GetMaxPv().ToString() + " PV";
            mvmt.text = p.GetMaxMvmt().ToString() + " cases / tour";
            ability.text = "Capacité : " + ToAbility(p.GetSpec());

            // increment position and change parent size when needed
            x += 220;
            count += 220;
            if (count > 1150)
            {
                RectTransform rt = parent.GetComponent<RectTransform>();
                rt.sizeDelta += new Vector2(220f, 0);
                x -= 110;
            }
        }
        parent.transform.localPosition += new Vector3(count / 2, 0, 0);
    }

    public void Cancel()
    {
        RectTransform rt = parent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1150f, 400f);
    }

    public string ToAbility(PenguinSpec spe)
    {
        string ability = "";
        switch (spe)
        {
            case PenguinSpec.plongeur:
                ability = "Nage";
                break;
            case PenguinSpec.soigneur:
                ability = "Soin";
                break;
            case PenguinSpec.constructeur:
                ability = "Construction";
                break;
            case PenguinSpec.pondeur:
                ability = "Vol";
                break;
            default:
                break;
        }
        return ability;
    }

    public void SetPenguinsInteractable()
    {
        foreach (Button b in buttons)
        {
            b.interactable = true;
        }
    }
}
