﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForSaleListBehaviour : MonoBehaviour {

    public QGManager manager;
    public GameObject roomDetailPrefab;
    public GameObject parent;
    private GameObject[] elements;
    private List<Room> availableRooms = new List<Room>();

    public void Init ()
    {
        availableRooms.Clear();
        availableRooms.Add(new Room(RoomType.coffemachine));
        availableRooms.Add(new Room(RoomType.icecreamfactory));
        int count = 0;
        float x = -455;
        foreach (Room m in this.availableRooms)
        {
            GameObject r = Instantiate(roomDetailPrefab, parent.transform);
            r.transform.localScale = new Vector3(1f, 1f, 1f);
            r.transform.localPosition = new Vector3(x, 0, 0);

            //set attributes
            Transform child = r.transform.Find("Name");
            Text name = child.GetComponent<Text>();
            child = r.transform.Find("Description");
            Text type = child.GetComponent<Text>();
            child = r.transform.Find("Price");
            Text price = child.GetComponent<Text>();
            name.text = RoomTypeToString(m.GetRoomType());
            type.text = m.GetDescription();
            price.text = "Coût :\n500 sorbets";

            // increment position and change parent size when needed
            x += 220;
            count += 220;
            if (count > 1150)
            {
                RectTransform rt = parent.GetComponent<RectTransform>();
                rt.sizeDelta += new Vector2(220f, 0);
                x -= 110;
            }
        }
        parent.transform.localPosition += new Vector3(count / 2, 0, 0);
    }

    public void Cancel()
    {
        RectTransform rt = parent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1150f, 400f);
    }

    public string RoomTypeToString(RoomType r)
    {
        switch (r)
        {
            case RoomType.coffemachine:
                return "Machine à café";
            case RoomType.icecreamfactory:
                return "Atelier à sorbets";
            default:
                return "";
        }
    }
}
