﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room {

    private RoomType type = RoomType.onsale;
    private string description;

	//constructors
    public Room()
    {
        this.type = RoomType.onsale;
        SetDescription();
    }

    public Room(RoomType type)
    {
        this.type = type;
        SetDescription();
    }

    private void SetDescription()
    {
        switch (this.type)
        {
            case RoomType.coffemachine:
                this.description = "Bonus :\n+ 10 % de productivité des scientifiques\n(I.C.E.B.E.R.G. + 0.5 % / jour)";
                break;
            case RoomType.icecreamfactory:
                this.description = "Bonus :\n + 25 sorbets / jour";
                break;
            default:
                this.description = "";
                break;
        }
    }

    //getters
    public RoomType GetRoomType()
    {
        return this.type;
    }

    public string GetDescription()
    {
        return this.description;
    }

    //setters
    public void SetType(RoomType type)
    {
        this.type = type;
    }
}
