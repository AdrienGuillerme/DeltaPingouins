﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseButtonBehaviour : MonoBehaviour {

    public Button parent;

	void Start () {
        parent.interactable = false;
	}
}
