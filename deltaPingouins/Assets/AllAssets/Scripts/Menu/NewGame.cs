﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class NewGame : MonoBehaviour {

    public static string username;

    public void StartGame(InputField input) {

        username = input.text;
        if (username != "")
        {
            LoadManager.playerName = username;
            LoadManager.previousLevel = "menu";
            LoadManager.LevelToLoad = "Intro";
            SceneManager.LoadScene("loading");
        }
    }
}
