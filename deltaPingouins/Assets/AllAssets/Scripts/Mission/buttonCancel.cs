﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonCancel : MonoBehaviour {

    public GameObject manager;

	public void OnClick()
    {
        manager.GetComponent<BoardGameManager>().ChangeBoardActivity();
    }
}
