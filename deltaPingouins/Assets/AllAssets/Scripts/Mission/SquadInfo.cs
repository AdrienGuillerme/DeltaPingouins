﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SquadInfo : MonoBehaviour {
    public GameObject ImageSelectedFighter;
    public GameObject[] ImageSquad;
    public Text NbTurns;

    private GameObject[] squad = null;

    private void Awake()
    {
        NbTurns.text = "Tour 0";
    }

    public void SetNbTurns(int turn)
    {
        NbTurns.text = "Tour " + turn;
    }

    public void setSquad(GameObject[] squad)
    {
        this.squad = squad;
        foreach (var squadMember in ImageSquad)
            squadMember.SetActive(false);

        int i = 0;
        foreach (var fighter in squad)
        {
            if (i >= ImageSquad.Length)
                break;

            ImageSquad[i].SetActive(true);
            ImageSquad[i].GetComponent<imageFighter>().setFighter(fighter);

            i++;
        }
    }

    public void updateSelectedFighter(GameObject figther)
    {  // update info displayed for selected fighter and squad
        ImageSelectedFighter.SetActive(true);
        ImageSelectedFighter.GetComponent<FighterSelectedInfo>().setFighter(figther);
        
        if (squad == null)
            return;

        int i = 0;
        foreach(var fighter in squad)
        {
            if (i >= ImageSquad.Length)
                break;

            ImageSquad[i].SetActive(true);
            ImageSquad[i].GetComponent<imageFighter>().setFighter(fighter); 

            i++;
        }
    }

}
