﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface boardInterface {
    GameObject getSnow();
    GameObject getIce();
    GameObject getWater();
    GameObject getDirt();
    GameObject getObstacle();
    GameObject getWall();
    GameObject getEnnemy();
    GameObject getHero();


    void enterTile(int x, int y);

    void exitTile(int x, int y);

    void ClickTile(int x, int y);
}
