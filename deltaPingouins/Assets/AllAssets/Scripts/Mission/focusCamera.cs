﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class focusCamera : MonoBehaviour {
    public int speed = 4;
    private Rigidbody2D rb2d = null;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private Vector2 destination;
    private bool moving = false;
	public void changePosition(int x, int y)
    {
        if(rb2d == null)
            rb2d = GetComponent<Rigidbody2D>();
        destination = new Vector2(x, y);
        Vector2 pos = rb2d.position;
        rb2d.velocity = new Vector2(destination.x - pos.x, destination.y - pos.y).normalized * speed;
        moving = true;
    }

    private void Update()
    {
        if(moving)
        {
            if (pathFinding.Distance(destination, rb2d.position) < 0.2)
            {
                rb2d.position = destination;
                rb2d.velocity = new Vector2(0, 0);
                moving = false;
            }
        }
        else
        {
            float xAxisValue = Input.GetAxis("Horizontal") / 2;
            float yAxisValue = Input.GetAxis("Vertical") / 2;

            rb2d.position = new Vector2(rb2d.position.x + xAxisValue, rb2d.position.y + yAxisValue);

            var delta = Input.GetAxis("Mouse ScrollWheel");
            if (delta > 0 && GetComponent<Camera>().orthographicSize > 4)
                GetComponent<Camera>().orthographicSize--;
            if (delta < 0 && GetComponent<Camera>().orthographicSize < 20)
                GetComponent<Camera>().orthographicSize++;
        }
            
    }
}
