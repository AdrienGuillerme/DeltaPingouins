﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoardGameManager : MonoBehaviour, boardInterface
{
    public GameObject ice;
    public GameObject snow;
    public GameObject water;
    public GameObject dirt;
    public GameObject obstacle;
    public GameObject wall;
    public GameObject panelEndGame;
    public UnityEngine.UI.Text textPanelEndGame;
    public GameObject panelPausedGame;
    public UnityEngine.UI.Text textPanelPausedGame;

    public GameObject PrefabRocket;
    public GameObject canvasUI;

    // TODO : replace heroes and ennemies by list of sprites
    public GameObject[] heroes;
    private int indexHeroes = 0;
    //public GameObject hero;
    public GameObject[] ennemies;
    private int indexEnnemies = 0;
    //public GameObject ennemy;

        // allow the script to change the camera location
    public GameObject Camera;

    private Map map = new Map();
    private fighter selectedFighter;

    private bool isPaused = false;
    private int turns = 0;
    private Vector2[] pathFighter = null;
    private GameObject line = null;
    private bool gameWon = false;
    private bool waitingRocket = false;
    private GameObject rocket = null;

    private GameObject[] queueFighters = null;
    private int adjustTurn = 0; // take count of killed fighters for turns

    private bool iAActing = false;
    private bool EndTurn = false;

    // 0 : walk, 1-2 : abilities
    private int actualState = 0;

    private void setColorTile(int x, int y, Color? color = null)
    {
        map.tiles[x][y].GetComponent<selectedTile>().spriteRenderer.color = color ?? new Color((float)0.2, 1, (float)0.2);
    }

    void Awake()
    {
        try
        {
           if(LoadManager.mapName.Equals(""))
                loadMap("demo.map");
           else
                loadMap(LoadManager.mapName);
        }
        catch (Exception excp)
        {
            Debug.Log("no map found");
            LoadManager.LevelToLoad = LoadManager.previousLevel;
            LoadManager.previousLevel = "mission";
            SceneManager.LoadScene("loading");
        }
    }
    private void loadMap(string nameMap)
    {
        if (File.Exists("./maps/" + nameMap))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("./maps/" + nameMap, FileMode.Open);
            map.Deserialize((serialisableMap)bf.Deserialize(file), this);
            file.Close();
        }
        else
        {
            Debug.Log("can't find the map");
            LoadManager.LevelToLoad = LoadManager.previousLevel;
            LoadManager.previousLevel = "mission";
            SceneManager.LoadScene("loading");
            return;
        }

        canvasUI.GetComponent<SquadInfo>().setSquad(map.Heroes.ToArray());

        if (map.Heroes.Count >= 1)
            selectFighter(map.Heroes[0]);
        else
            selectFighter(map.Ennemies[0]);

        List<GameObject> queue = new List<GameObject>();

        foreach(var fighter in map.Heroes)
        {
            fighter.GetComponent<fighter>().manager = this;
            queue.Add(fighter);
        }
        foreach (var fighter in map.Ennemies)
        {
            fighter.GetComponent<fighter>().manager = this;
            queue.Add(fighter);
        }
        queueFighters = queue.ToArray();
        canvasUI.SetActive(true);
        if(map.Ennemies.Count < 1 && map.Heroes.Count <1)
        {
            Debug.Log("no ennemies or heroes found");
            LoadManager.LevelToLoad = LoadManager.previousLevel;
            LoadManager.previousLevel = "mission";
            SceneManager.LoadScene("loading");
        }
        WinOrLose(); // will stop the game if no ennemy/hero can be found
    }

    public void KillFighters()
    {  // todo : add animation
        List<GameObject> toBeRemove = new List<GameObject>();
        bool changeTurn = false;
        foreach (var fighter in map.Heroes)
        {
            if(fighter.GetComponent<fighter>().HP <= 0)
            {
                toBeRemove.Add(fighter);
            }
        }
        foreach(var fighter in toBeRemove)
        {
            if (fighter.GetComponent<fighter>() == selectedFighter)
                changeTurn = true;
            map.Heroes.Remove(fighter);
            UnityEngine.Object.Destroy(fighter, 1);
        }

        toBeRemove.Clear();
        foreach (var fighter in map.Ennemies)
        {
            if (fighter.GetComponent<fighter>().HP <= 0)
            {
                toBeRemove.Add(fighter);
            }
        }
        foreach (var fighter in toBeRemove)
        {
            if (fighter.GetComponent<fighter>() == selectedFighter)
                changeTurn = true;

            map.Ennemies.Remove(fighter);            
            UnityEngine.Object.Destroy(fighter, 1);
        }
        canvasUI.GetComponent<SquadInfo>().setSquad(map.Heroes.ToArray());
        WinOrLose();
        if (changeTurn)
            ChangeTurn(false);
    }

    private void selectFighter(GameObject fighter)
    {
        canvasUI.GetComponent<SquadInfo>().updateSelectedFighter(fighter);
        
        selectedFighter = fighter.GetComponent<fighter>();
        Camera.GetComponent<focusCamera>().changePosition(selectedFighter.getX(),
            selectedFighter.getY());
    }

    private void moveState(int x, int y)
    {
        pathFighter = selectedFighter.findPath(x, y, map, selectedFighter.MP);
        if (pathFighter != null)
            colorPathFighter(new Color(0,1,0));

        setColorTile(x, y, new Color((float)0.2, 1, (float)0.8));
    }

    private void pushState(int x, int y)
    {  // show the 4 tiles accessible
        colorPathFighter();
        foreach (var tile in pathFighter)
            if (tile.x == x && tile.y == y && map.FindFigtherAt(x, y) != null)
            {
                setColorTile(x, y, new Color(1, (float)0.2, (float)0.8));
                return;
            }
        setColorTile(x, y, new Color((float)0.2, 1, (float)0.8));
    }

    private void destroyLine()
    {
        if(line != null)
            GameObject.Destroy(line);
    }

    private GameObject DrawLine(Vector2 start, Vector2 end, Color color)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.1f, 0.1f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        return myLine;
    }
    private void fireState(int x, int y)
    // show all the tiles in the range accessible and a line between center 
    // of both tiles begin and end and the 5 tiles touched by explosion
    {
        destroyLine();

        pathFighter = selectedFighter.ReachableTilesBlastRocket(x, y, map);
        if(pathFighter != null)
        {
            colorPathFighter(new Color((float)0.5, (float)0.4, (float)0.1));
            line = DrawLine(selectedFighter.getPosition(), new Vector2(x, y), new Color(1, (float)0.8, 0));
        }
    }
    // methodes called by tiles when interacted with
    public void enterTile(int x, int y)
    {
        if (isPaused || selectedFighter.isMoving || gameWon || waitingRocket)
            return;

        resetPathFighter();

        setInfoFigter(x, y);

        switch(actualState)
        {
            case 0:
                moveState(x, y);
                break;
            case 1:
                pushState(x, y);
                break;
            case 2:
                fireState(x, y);
                break;
        }
    }

    private void resetPathFighter()
    {
        if (pathFighter != null)
            foreach (var tile in pathFighter)
                map.tiles[(int)tile.x][(int)tile.y].GetComponent<selectedTile>().resetColor();
    }

    private void colorPathFighter(Color? color = null)
    {
        foreach (var tile in pathFighter)
            setColorTile((int)tile.x, (int)tile.y, color ?? new Color((float)0.2, 1, (float)0.8));
    }

    public void exitTile(int x, int y)
    {
        map.tiles[x][y].GetComponent<selectedTile>().resetColor();
    }
    
    private void moveAction(int x, int y)
    {
        selectedFighter.moveTo(selectedFighter.findPath(x, y, map, selectedFighter.MP));
    }
    private void pushAction(int x, int y)
    {
        GameObject target = map.FindFigtherAt(x, y);
        if (target == null || (selectedFighter.getX() == x && selectedFighter.getY() == y ))
            return;

        EndTurn = true;
        target.GetComponent<fighter>().GetPunched(selectedFighter);
    }
    private void fireAction(int x, int y)
    { // launch a projectile
        destroyLine();
        resetPathFighter();

        if (selectedFighter.ReachableTilesBlastRocket(x, y, map) == null)
            return;

        waitingRocket = true;
        rocket = Instantiate(PrefabRocket, new Vector3(selectedFighter.getX(), selectedFighter.getY(), 0f), Quaternion.identity) as GameObject;

        rocket.GetComponent<Rocket>().manager = this;
        rocket.GetComponent<Rocket>().damage = selectedFighter.damageRocket;
        rocket.GetComponent<Rocket>().VelocityRocket = 6;
        rocket.GetComponent<Rocket>().SetDestination(new Vector2(x, y));
    }
    public void RocketExplosion(Vector2 coords, int damage)
    {
        // todo : damage tiles, kill walkers and damage fighters in the blast
        // and resume game

        if (gameWon)
            return;

        this.Camera.GetComponent<PlaySound>().Play("explosion");

        Vector2[] blast = selectedFighter.ReachableTilesBlastRocket((int)coords.x, (int)coords.y, map, false);
        foreach(var tile in blast)
        {
            DamageTile((int)tile.x, (int)tile.y);
            GameObject fighter = map.FindFigtherAt((int)tile.x, (int) tile.y);
            if (fighter != null)
            {
                fighter.GetComponent<fighter>().GetBlasted(damage);
            }
        }

        UnityEngine.Object.Destroy(rocket ,0);
        if(!isPaused)
            canvasUI.SetActive(true);
        waitingRocket = false;
        KillFighters();
        ChangeTurn();
    }
    public void ClickTile(int x, int y)
    {
        if (isPaused || selectedFighter.isMoving || gameWon || waitingRocket)
            return;

        resetPathFighter();

        switch (actualState)
        {
            case 0:
                moveAction(x, y);
                break;
            case 1:
                pushAction(x, y);
                break;
            case 2:
                fireAction(x, y);
                break;
        }              
    }
    // end methodes called by tiles when interacted with

    private void setInfoFigter(int x, int y)
    {
        GameObject fighterInfo = map.FindFigtherAt(x, y);
        if (fighterInfo == null)
            return;

        canvasUI.GetComponent<InfoFighter>().SetFighter(fighterInfo);
    }


    private void DamageTile(int x, int y)
    {
        switch ((typeToPlace) map.tiles[x][y].GetComponent<selectedTile>().typeTile)
        {
            default:
                return;
            case typeToPlace.ice:
                UnityEngine.Object.Destroy(map.tiles[x][y]);
                map.tiles[x][y] =
                    Instantiate(water, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                map.tiles[x][y].GetComponent<selectedTile>().typeTile = (int)typeToPlace.water;
                break;
            case typeToPlace.snow:
                UnityEngine.Object.Destroy(map.tiles[x][y]);
                map.tiles[x][y] =
                    Instantiate(ice, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                map.tiles[x][y].GetComponent<selectedTile>().typeTile = (int)typeToPlace.ice;
                break;
        }
        map.tiles[x][y].GetComponent<selectedTile>().board = this;
        map.tiles[x][y].GetComponent<selectedTile>().x = x;
        map.tiles[x][y].GetComponent<selectedTile>().y = y;
    }
    private void RepairTile(int x, int y)
    {
        switch ((typeToPlace)map.tiles[x][y].GetComponent<selectedTile>().typeTile)
        {
            default:
                return;
            case typeToPlace.water:
                UnityEngine.Object.Destroy(map.tiles[x][y]);
                map.tiles[x][y] =
                    Instantiate(ice, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                map.tiles[x][y].GetComponent<selectedTile>().typeTile = (int)typeToPlace.ice;

                break;
            case typeToPlace.ice:
                UnityEngine.Object.Destroy(map.tiles[x][y]);
                map.tiles[x][y] =
                    Instantiate(snow, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                map.tiles[x][y].GetComponent<selectedTile>().typeTile = (int)typeToPlace.snow;

                break;
        }
        map.tiles[x][y].GetComponent<selectedTile>().board = this;
        map.tiles[x][y].GetComponent<selectedTile>().x = x;
        map.tiles[x][y].GetComponent<selectedTile>().y = y;
    }

    public void ChangeTurn(bool incrementTurn = true)
    {
        if (gameWon)
            return;
        
        if (selectedFighter.isMoving || waitingRocket)
        {
            EndTurn = true;
            return;
        }
        EndTurn = false;

        if (incrementTurn)
           canvasUI.GetComponent<SquadInfo>().SetNbTurns(++turns);


        /*
        int nbPlayers = map.Heroes.Count + map.Ennemies.Count;
        int moduloTurn = turns % nbPlayers;
        List<GameObject> fighters = new List<GameObject>();
        foreach (var h in map.Heroes)
            fighters.Add(h);
        foreach (var e in map.Ennemies)
            fighters.Add(e);

        selectFighter(fighters[moduloTurn]);
        */
        List<GameObject> fighters = new List<GameObject>();
        foreach (var h in map.Heroes)
            fighters.Add(h);
        foreach (var e in map.Ennemies)
            fighters.Add(e);
        int i = 0;
        GameObject nextFighter = null;
        while(i < queueFighters.Length)
        {
            nextFighter = queueFighters[(turns + adjustTurn + i) % queueFighters.Length];
            if (fighters.Contains(nextFighter))
                break;
            i++;
        }
        selectFighter(nextFighter);
        adjustTurn += i;
        

        selectedFighter.NewTurn();
        ChangeState();
        if (map.Ennemies.Contains(nextFighter))
        //if (map.Ennemies.Contains(fighters[moduloTurn]))
            IAAction();
    }

    private void IAAction()
    // actions of the selected ennemy
    {
        resetPathFighter();
        iAActing = true;
        EndTurn = false;
        canvasUI.SetActive(false);

        foreach (var hero in map.Heroes)
        {
            // don't use IA on heroes
            if (hero.GetComponent<fighter>() == selectedFighter)
            {
                if (!isPaused)
                    canvasUI.SetActive(true);
                iAActing = false;
                return;
            }
        }

        // ### if hero at close range : punch and endTurn ####
        pathFighter = selectedFighter.ReachableTilesPunch(map);
        GameObject fighterInRange = null;
        foreach (var tile in pathFighter)
        {
            fighterInRange = map.FindFigtherAt((int)tile.x, (int)tile.y, false);
            if(fighterInRange != null)
            {
                break;
            }
        }
        // punch
        if (fighterInRange != null)
        {
            EndTurn = true;
            fighterInRange.GetComponent<fighter>().GetPunched(selectedFighter);
            return;
        }

        // ### if a hero is in range, go to close range ###
        // check all reachable tiles
        pathFighter = selectedFighter.ReachableTilesMove(map, false);
        Vector2[] path = null;
        foreach (var tile in pathFighter)
        {
            // check if an adversaire is in the range
            fighterInRange = map.FindFigtherAt((int)tile.x, (int)tile.y, false);
            if (fighterInRange != null) // check if a path can be found
            {
                path = selectedFighter.findPath((int)tile.x, (int)tile.y, map, selectedFighter.MP);
            }
            if (path != null)
                break;
        }
        if (path != null)
        {
            // move to the decided location
            EndTurn = false;
            selectedFighter.moveTo(path);
            return;
        }
        
        
        // ### if hero at rocket range : fire ###
        pathFighter = selectedFighter.ReachableTilesRocket(map);
        foreach (var tile in pathFighter)
        {
            fighterInRange = map.FindFigtherAt((int)tile.x, (int)tile.y, false);
            if (fighterInRange != null)
            {
                break;
            }
        }
        // fire
        if (fighterInRange != null)
        {
            EndTurn = true;
            fighter classFighter = fighterInRange.GetComponent<fighter>();
            fireAction(classFighter.getX(), classFighter.getY());
            return;
        }

        // ### no hero at range, stay ####
        if(!isPaused)
            canvasUI.SetActive(true);
        iAActing = false;
        EndTurn = true;
        ChangeTurn();
    }

    private void endTurn()
    {
        if (gameWon)
            return;
        EndTurn = false;
        iAActing = false;
        if(!isPaused)
            canvasUI.SetActive(true);
        ChangeTurn();
    }

    public void ReloadState()
    {
        if (gameWon)
            return;
        if (!iAActing)
        {
            Camera.GetComponent<focusCamera>().changePosition(selectedFighter.getX(), selectedFighter.getY());
            ChangeState(actualState);
            if(!isPaused)
                canvasUI.SetActive(true);
        }
        if (EndTurn)
            endTurn();
        else
            if (iAActing)
                IAAction();
    }
    public void ChangeState(int newState = 0)
    {
        if (gameWon)
            return;
        actualState = newState;
        resetPathFighter();
        destroyLine();

        if (selectedFighter.isMoving)  // avoid printing an obsolete path
            return;

        // todo : implement
        switch (actualState)
        {
            case 1:
                // ability 1
                pathFighter = selectedFighter.ReachableTilesPunch(map);
                colorPathFighter();
                break;
            case 2:
                // ability 2
                //pathFighter = selectedFighter.ReachableTilesRocket(map);
                //colorPathFighter();
                break;
            default:
                // walk
                pathFighter = selectedFighter.ReachableTilesMove(map);
                colorPathFighter();
                break;
        }
    }


    public void ChangeBoardActivity(bool endGame = false)
    // prevent or allow user to interact win board when paused or ended game
    {
        resetPathFighter();
        if (gameWon)
        {
            canvasUI.SetActive(false);
            return;
        }
        gameWon = endGame;

        isPaused = !isPaused;
        if(!isPaused)
            panelPausedGame.SetActive(false);
        canvasUI.SetActive(!isPaused);
    }
    private void WinOrLose()
    {
        if (map.Ennemies.Count > 0 && map.Heroes.Count > 0)
            return;

        if (map.Ennemies.Count <= 0)
        {
            textPanelEndGame.text = "Victoire!";
            this.Camera.GetComponent<PlaySound>().Play("win");
        }

        if (map.Heroes.Count <= 0)
        {
            textPanelEndGame.text = "Défaite !";
            this.Camera.GetComponent<PlaySound>().Play("lose");
        }
        gameWon = true;
        ChangeBoardActivity(true);
        if(!isPaused)
            panelEndGame.SetActive(true);
    }
    private void PauseGame()
    {
        this.Camera.GetComponent<PlaySound>().Play("flee");
        if(!isPaused)
            panelPausedGame.SetActive(true);
        ChangeBoardActivity();
        textPanelPausedGame.text = "Retour menu ? \n (la mission ne sera pas sauvegardée)";
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
                ChangeBoardActivity();
            else
                PauseGame();
        }
    }

    // getters
    public GameObject getSnow()
    {
        return snow;
    }
    public GameObject getIce()
    {
        return ice;
    }
    public GameObject getWater()
    {
        return water;
    }
    public GameObject getDirt()
    {
        return dirt;
    }
    public GameObject getObstacle()
    {
        return obstacle;
    }
    public GameObject getWall()
    {
        return wall;
    }
    public GameObject getHero()
    {
        indexHeroes = (indexHeroes + 1) % heroes.Length;
        return heroes[indexHeroes];
    }
    public GameObject getEnnemy()
    {
        indexEnnemies = (indexEnnemies + 1) % ennemies.Length;
        return ennemies[indexEnnemies];
    }
    public Map GetMap()
    {
        return map;
    }
}
