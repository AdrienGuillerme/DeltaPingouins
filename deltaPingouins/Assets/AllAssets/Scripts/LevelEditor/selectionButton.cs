﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectionButton : MonoBehaviour
{
    public boardManager manager;
    public string type = "ground";

    private void Awake()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        manager.changeTypeToPlace(type);
    }
}
